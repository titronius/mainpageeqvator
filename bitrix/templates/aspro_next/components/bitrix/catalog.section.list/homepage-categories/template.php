<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
$arParams['VIEW_MODE'] = "LIST";

?>
<?if ($arResult["SECTIONS"]):?>
    <div class="eq-row eq-row_flex">
        <? $counter = 0;
        $max_counter = 10; ?>
        <? foreach ($arResult['SECTIONS'] as &$arItems) : ?>
            <?
            if ($counter >= $max_counter) {
                break;
            }
            ?>
            <div class="homepage-category-thumb">
                <div class="homepage-category-thumb__head">
                    <a href="<?= $arItems["SECTION_PAGE_URL"] ?>" class="homepage-category-thumb__link-wrapper">
                        <?
                        $img = CFile::ResizeImageGet($arItems['DETAIL_PICTURE'], Array('width'=>650, 'height'=>500), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                        ?>
                        <div class="homepage-category-thumb__image-box"
                             style="background-image: url(<?= $img["src"]; ?>)"></div>
                        <div class="homepage-category-thumb__heading">
                            <div class="homepage-category-thumb__title"><?= $arItems['NAME']; ?></div>
                            <div class="homepage-category-thumb__counter">
                                <? if ($arParams["COUNT_ELEMENTS"]) : ?>
                                    <span class="homepage-category-thumb__total"><?= $arItems['ELEMENT_CNT']; ?> в&nbsp;каталоге </span>
                                <? endif; ?>
                                <!-- <span class="homepage-category-thumb__in-stock"> в&nbsp;наличии</span> -->
                            </div>
                        </div>
                    </a>
                </div>
                <? if ($arItems["SECTIONS"] && $counter < 5): ?>

                    <div class="homepage-category-thumb__body">
                        <div class="homepage-category-thumb__subcategories">
                            <div class="homepage-category-thumb__subcategories-row">
                                <? foreach ($arItems["SECTIONS"] as $arItem): ?>
                                    <a href="<?= $arItem["SECTION_PAGE_URL"] ?>"
                                       class="homepage-category-thumb__subcategories-link"><?= $arItem["NAME"] ?></a>
                                    <span class="homepage-category-thumb__subcategories-separator">·</span>
                                <? endforeach; ?>
                            </div>
                        </div>
                    </div>

                <? endif; ?>
            </div>
            <? $counter++; ?>
        <? endforeach; ?>
    </div>
<?endif;?>
