<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?
global $arTheme;
$iVisibleItemsMenu = ($arTheme['MAX_VISIBLE_ITEMS_MENU']['VALUE'] ? $arTheme['MAX_VISIBLE_ITEMS_MENU']['VALUE'] : 10);
?>
<? if ($arResult || $arTheme['SHOW_CALLBACK']['VALUE'] == 'Y'): ?>
    <nav class="header-menu header-menu_top-b">
        <ul class="header-menu__list">
<!--            --><?// if ($arTheme['SHOW_CALLBACK']['VALUE'] == 'Y'): ?>
<!--                <li class="header-menu__item header-menu__item_for-video-call">-->
<!--                    <a class="header-button header-button_accent" title="Заказать звонок">-->
<!--                                <span class="header-button__icon">-->
<!--                                <svg width="24" height="24" fill="none" id="svg-video-eye"-->
<!--                                     xmlns="http://www.w3.org/2000/svg"><path-->
<!--                                            d="M8 6c3.3 0 6 2.7 6 6s-2.7 6-6 6-6-2.7-6-6 2.7-6 6-6zm0-2c-4.4 0-8 3.6-8 8s3.6 8 8 8 8-3.6 8-8-3.6-8-8-8zm13 5c-1.65 0-3 1.35-3 3s1.35 3 3 3 3-1.35 3-3-1.35-3-3-3zM8 7.5c-2.5 0-4.5 2-4.5 4.5s2 4.5 4.5 4.5 4.5-2 4.5-4.5-2-4.5-4.5-4.5zm1.5 4c-.55 0-1-.45-1-1s.45-1 1-1 1 .45 1 1-.45 1-1 1z"></path></svg>-->
<!--                                </span>-->
<!--                        <span data-eyezon="5fb28a615ce3fe652398f80f"-->
<!--                              class="header-button__label off-on-small-pc">Видеозвонок</span>-->
<!--                    </a>-->
<!---->
<!--                </li>-->
<!--            --><?// endif; ?>

            <li class="header-menu__item header-menu__item_for-video-call">
                <a class="header-button" title="Заказать звонок">
                    <span data-event="jqm" data-param-form_id="CALLBACK" data-name="callback"
                          class="header-button__label">Заказать звонок</span>
                </a>

            </li>

            <? foreach ($arResult as $arItem): ?>
                <li class="header-menu__item">
                    <a href="<?= $arItem["LINK"] ?>" class="header-button" title="<?= $arItem["TEXT"] ?>">
                        <span class="header-button__label"><?= $arItem["TEXT"] ?></span>
                    </a>
                </li>
            <? endforeach; ?>
            <li class="header-menu__item">
                <?= CNext::ShowCabinetLinkNew(true, true); ?>
            </li>
        </ul>
    </nav>
<? endif; ?>