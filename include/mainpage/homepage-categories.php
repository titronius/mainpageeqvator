<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="section section_categories">
    <div class="box">

        <div class="promo-heading">
            <h2 class="promo-heading__title">Тысячи позиций оборудо&shy;вания и&nbsp;комплекту&shy;ющих</h2>
        </div>

        <?
        $APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"homepage-categories", 
	array(
		"COMPONENT_TEMPLATE" => "homepage-categories",
		"IBLOCK_TYPE" => "aspro_next_catalog",
		"IBLOCK_ID" => "114",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "2",
		"SECTION_FIELDS" => array(
			0 => "ID",
			1 => "CODE",
			2 => "NAME",
			3 => "SORT",
			4 => "PICTURE",
			5 => "DETAIL_PICTURE",
			6 => "",
		),
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"VIEW_MODE" => "LINE",
		"SHOW_PARENT_NAME" => "Y",
		"SECTION_URL" => "#SECTION_CODE_PATH#/",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);
        ?>
        <div class="eq-row eq-row_block-footer">
            <div class="border-for-block-footer"></div>
            <a href="/catalog/" class="button button_large button_color-a">
                <span class="button__label">Перейти в каталог</span>
            </a>
        </div>

    </div>
</div>
