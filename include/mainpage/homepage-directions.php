<div class="section section_directions">
    <div class="box">

        <div class="promo-heading">
            <h2 class="promo-heading__title">Поможем выбрать! Поможем разобраться!</h2>
        </div>



        <div class="eq-row eq-row_flex">

            <div class="direction-block">
                <div class="direction-block__head">
                    <img src="/images/media/illustrations/for-directions/engineering.png" alt="" class="direction-block__image">
                </div>
                <div class="direction-block__body">
                    <div class="direction-block__heading">Подбор</div>
                    <div class="direction-block__text">Помогаем разобраться в&nbsp;системах отопления, водоснабжения, автополива. Готовим схемы и&nbsp;документацию, индивидуально подбираем оборудование и&nbsp;комплектующие.</div>
                </div>
            </div>
            <div class="direction-block">
                <div class="direction-block__head">
                    <img src="/images/media/illustrations/for-directions/trade.png" alt="" class="direction-block__image">
                </div>
                <div class="direction-block__body">
                    <div class="direction-block__heading">Комплекта&shy;ция</div>
                    <div class="direction-block__text">Всегда в&nbsp;наличии в&nbsp;Краснодаре широкий <a href="https://eqvator.ru/catalog/">ассортимент сертифицированного оборудования</a> от <a href="https://eqvator.ru/info/brands/">проверенных производителей</a>. С&nbsp;<a href="https://eqvator.ru/help/warranty/">гарантией</a> и&nbsp;<a href="https://eqvator.ru/help/#kak-poluchit">доставкой.</a></div>
                </div>
            </div>
            <div class="direction-block">
                <div class="direction-block__head">
                    <img src="/images/media/illustrations/for-directions/service.png" alt="" class="direction-block__image">
                </div>
                <div class="direction-block__body">
                    <div class="direction-block__heading">Обслужи&shy;вание</div>
                    <div class="direction-block__text">Наш партнёр, СЦ "Аквайдер", обеспечивает гарантийное и постгарантийное обслуживание продаваемого оборудования. Мы, также, поддерживаем запас оригинальных запчастей.</div>
                </div>
            </div>

        </div>
    </div>
</div>