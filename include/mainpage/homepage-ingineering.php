<div class="section section_engineering">
    <div class="box">

        <div class="promo-heading">
            <h2 class="promo-heading__title">Технический отдел</h2>
            <div class="promo-heading__description">Помогаем разобраться в&nbsp;сложных системах. Консультируем, разрабатываем схемы монтажа, готовим документацию, выполняем технические расчёты.</div>
        </div>

        <div class="eq-row eq-row_flex">
            <div class="illustration-frame">
                <div class="illustration-background illustration-background_e"></div>
                <img src="/images/media/illustrations/engineering_1.svg" class="direction-illustration default" alt="">
                <img src="/images/media/illustrations/engineering_2.svg" class="direction-illustration adaptive" alt="">
            </div>
            <div class="direction-description">
                <div class="simple-txt-block">
                    <div class="simple-txt-block__title">Консультируем</div>
                    <div class="simple-txt-block__descr">Отвечаем на вопросы об отоплении и водоснабжении.</div>
                </div>
                <div class="simple-txt-block">
                    <div class="simple-txt-block__title">Разрабатываем схемы монтажа</div>
                    <div class="simple-txt-block__descr">Составляем принципиальные схемы монтажа. На выходе выдаём список оборудования и комплектующих, простые и ясные указания что, как и где монтировать.</div>
                </div>
                <div class="simple-txt-block">
                    <div class="simple-txt-block__title">Выполняем Теплотехнические расчёты</div>
                    <div class="simple-txt-block__descr">Определяем оптимальную мощность отопительной системы исходя из потенциальных теплопотерь здания, с учётом действующих норм.</div>
                </div>
                <div class="simple-txt-block">
                    <div class="simple-txt-block__title">Рассчитываем гидравлическую балансировку системы</div>
                    <div class="simple-txt-block__descr">Составляем гидравлический расчёт для обеспечения равномерного прогрева отопительного контура и эффективного использования мощности котла и насосов.</div>
                </div>
                <div class="simple-txt-block">
                    <div class="simple-txt-block__title">Занимаемся водоподготовкой</div>
                    <div class="simple-txt-block__descr">Бесплатно делаем базовый анализ воды. Подбираем оборудование и комплектующие для обеспечения требуемого качества воды.</div>
                </div>
                <div class="simple-txt-block">
                    <div class="simple-txt-block__title">Проекты систем автоматического полива</div>
                    <div class="simple-txt-block__descr">Производим замеры на местности. Чертим план участка, создаём дендроплан, проектируем систему автополива с учётом источников воды, имеющегося оборудования и дополнительных пожеланий. Выполняем гидравлические расчеты.</div>
                </div>
            </div>
        </div>

    </div>
</div>