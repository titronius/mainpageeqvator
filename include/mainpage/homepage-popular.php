<?// if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$arSelect = ["ID", "NAME", "DATE_ACTIVE_FROM"];
$arFilter = ["IBLOCK_ID" => 109, "ID" => 52161, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"];
$res = CIblockElement::GetList(
    [],
    $arFilter,
    false,
    [],
    ['ID', 'PROPERTY_LINK_GOODS']
);
$links = [];

while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $links[] = $arFields["PROPERTY_LINK_GOODS_VALUE"];
}
$GLOBALS['arrProductsFilterMainPage'] = array('ID' => $links);
?>

<?
$APPLICATION->IncludeComponent(
    "bitrix:catalog.top",
    "products_slider_custom_2021", //
    array(
        "IBLOCK_TYPE" => "aspro_next_catalog",
        "IBLOCK_ID" => "114",
        "TITLE" => "Хиты продаж",
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_ORDER" => "asc",
        "ELEMENT_COUNT" => count($links),
        "LINE_ELEMENT_COUNT" => "",
        "PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "OFFERS_LIMIT" => "10",
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "BASKET_URL" => SITE_DIR."basket/",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "3600000",
        "CACHE_GROUPS" => "N",
        "CACHE_FILTER" => "Y",
        "DISPLAY_COMPARE" => "Y",
        "PRICE_CODE" => [
            "Интернет-магазин"
        ],
        "USE_PRICE_COUNT" => "Y",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_PROPERTIES" => array(
        ),
        "CONVERT_CURRENCY" => "N",
        "FILTER_NAME" => "arrProductsFilterMainPage",
        "SHOW_BUY_BUTTONS" => "Y",
        "USE_PRODUCT_QUANTITY" => "N",
        "INIT_SLIDER" => "Y",
        "COMPONENT_TEMPLATE" => "products_slider",
        "ELEMENT_SORT_FIELD2" => "id",
        "ELEMENT_SORT_ORDER2" => "desc",
        "OFFERS_FIELD_CODE" => array(
            0 => "ID",
            1 => "NAME",
            2 => "",
        ),
        "OFFERS_PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "OFFERS_SORT_FIELD" => "sort",
        "OFFERS_SORT_ORDER" => "asc",
        "OFFERS_SORT_FIELD2" => "id",
        "OFFERS_SORT_ORDER2" => "desc",
        "SEF_MODE" => "N",
        "CACHE_FILTER" => "Y",
        "SHOW_MEASURE" => "Y",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "OFFERS_CART_PROPERTIES" => array(
        ),
        "COMPARE_PATH" => "",
        "SHOW_DISCOUNT_PERCENT" => "Y",
        "SHOW_OLD_PRICE" => "Y",
        "SHOW_RATING" => "N",
        "SHOW_MAX_QUANTITY" => "Y",
        "MESS_SHOW_MAX_QUANTITY" => "В наличии",
        "MESS_RELATIVE_QUANTITY_MANY" => "В наличии",
        "MESS_RELATIVE_QUANTITY_FEW" => "Под заказ",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "RELATIVE_QUANTITY_FACTOR" => 1,
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO"
    ),
    false, array("HIDE_ICONS" => "Y")
);
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>

