<div class="section section_service">
    <div class="box">

        <div class="promo-heading">
            <h2 class="promo-heading__title">Сервис, гарантия, ремонт</h2>
            <div class="promo-heading__description">Наш партнёр, сервисный центр «Аквайдер», выполняет работы по&nbsp;ремонту, запуску и&nbsp;сервисному обслуживанию оборудования. Мы, также, обеспечиваем постоянный складской запас запчастей и&nbsp;комплектующих.</div>
        </div>

        <div class="eq-row eq-row_flex">
            <div class="direction-description">
                <div class="simple-txt-block">
                    <div class="simple-txt-block__title">Запускаем и настраиваем</div>
                    <div class="simple-txt-block__descr">СЦ «Аквайдер»  выполняет первый запуск и настройку оборудования.</div>
                </div>
                <div class="simple-txt-block">
                    <div class="simple-txt-block__title">Обслуживаем</div>
                    <div class="simple-txt-block__descr">СЦ «Аквайдер» заключает договоры на сервисное обслуживание котлов, водонагревателей, тепловых пунктов, водонапорных станций и другого оборудования.</div>
                </div>
                <div class="simple-txt-block">
                    <div class="simple-txt-block__title">Ремонтируем</div>
                    <div class="simple-txt-block__descr">СЦ «Аквайдер» выполняет плановый и срочный ремонт оборудования большинства известных брендов. Многие работы специалисты способны выполнить на месте, для прочих случаев в Краснодаре есть десять точек приёма оборудования на ремонт.</div>
                </div>
                <div class="simple-txt-block">
                    <div class="simple-txt-block__title">Держим в наличии запчасти</div>
                    <div class="simple-txt-block__descr">На наших складах и складах СЦ «Аквайдер» постоянно поддерживается огромный запас оригинальных запчастей и комплектующих, что позволяет выполнять ремонтные работы в кратчайшие сроки.</div>
                </div>
            </div>
            <div class="illustration-frame">
                <div class="illustration-background illustration-background_s"></div>
                <img src="/images/media/illustrations/service_1.svg" class="direction-illustration default" alt="">
                <img src="/images/media/illustrations/service_2.svg" class="direction-illustration adaptive" alt="">
            </div>
        </div>
    </div>
</div>