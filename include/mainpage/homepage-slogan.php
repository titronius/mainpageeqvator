<div class="section section_slogan">
	<div class="box box_slogan">
		<div class="promo-heading">
			<h1 class="promo-heading__title">
				<span class="selection"> «Экватор»&nbsp;</span>— отопление, <span class="promo-heading__long-word">водо&shy;снабжение</span>,<br>
				фильтрация и&nbsp;автополив</h1>
		</div>
	</div>
</div>