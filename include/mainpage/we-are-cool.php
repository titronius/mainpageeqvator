<div class="section section_we-are-cool">
    <div class="box">

        <div class="promo-heading">
            <h2 class="promo-heading__title">
                <span class="selection">«Экватор»</span>&nbsp;— это комплексное решение задач отопления и водо&shy;снабжения.
            </h2>
        </div>


        <div class="eq-row eq-row_flex">
            <div class="feature-block">
                <div class="feature-block__aside">
                    <div class="feature-block__image-box">
                        <img src="/images/media/advantages/advantages_reputation.png" alt="" class="feature-block__image">
                    </div>
                </div>
                <div class="feature-block__body">
                    <div class="feature-block__title">Безупречная репутация</div>
                    <div class="feature-block__description">Работаем 22&nbsp;года. Оборудование и&nbsp;комплектующие только от&nbsp;проверенных поставщиков. Сертификаты на&nbsp;каждую позицию и&nbsp;гарантия качества.</div>
                </div>
            </div>
            <div class="feature-block">
                <div class="feature-block__aside">
                    <div class="feature-block__image-box">
                        <img src="/images/media/advantages/advantages_available.png" alt="" class="feature-block__image">
                    </div>
                </div>
                <div class="feature-block__body">
                    <div class="feature-block__title">Наличие товара</div>
                    <div class="feature-block__description">Всегда в&nbsp;наличии огромное количество позиций на&nbsp;собственных складах в&nbsp;Краснодаре.</div>
                </div>
            </div>
            <div class="feature-block">
                <div class="feature-block__aside">
                    <div class="feature-block__image-box">
                        <img src="/images/media/advantages/advantages_online.png" alt="" class="feature-block__image">
                    </div>
                </div>
                <div class="feature-block__body">
                    <div class="feature-block__title">Работа онлайн</div>
                    <div class="feature-block__description">Оперативно обрабатываем заявки. Консультируем по&nbsp;видеосвязи. Заказать и&nbsp;оплатить можно онлайн.</div>
                </div>
            </div>
            <div class="feature-block">
                <div class="feature-block__aside">
                    <div class="feature-block__image-box">
                        <img src="/images/media/advantages/advantages_payment.png" alt="" class="feature-block__image">
                    </div>
                </div>
                <div class="feature-block__body">
                    <div class="feature-block__title">Разные варианты оплаты</div>
                    <div class="feature-block__description">Наличный расчёт, банковские карты, безналичные переводы, платёжные системы, электронные кошельки.</div>
                </div>
            </div>
            <div class="feature-block">
                <div class="feature-block__aside">
                    <div class="feature-block__image-box">
                        <img src="/images/media/advantages/advantages_delivery.png" alt="" class="feature-block__image">
                    </div>
                </div>
                <div class="feature-block__body">
                    <div class="feature-block__title">Доставка</div>
                    <div class="feature-block__description">Бесплатно доставляем по&nbsp;Краснодару заказы от&nbsp;10&nbsp;000&nbsp;₽.</div>
                </div>
            </div>
            <div class="feature-block">
                <div class="feature-block__aside">
                    <div class="feature-block__image-box">
                        <img src="/images/media/advantages/advantages_documents.png" alt="" class="feature-block__image">
                    </div>
                </div>
                <div class="feature-block__body">
                    <div class="feature-block__title">Полный пакет документов</div>
                    <div class="feature-block__description">Предоставляем все необходимые документы. Работаем по&nbsp;договору, оформляем пакет документов для отчетности и&nbsp;вычета по НДС.</div>
                </div>
            </div>
            <div class="feature-block">
                <div class="feature-block__aside">
                    <div class="feature-block__image-box">
                        <img src="/images/media/advantages/advantages_bonuses.png" alt="" class="feature-block__image">
                    </div>
                </div>
                <div class="feature-block__body">
                    <div class="feature-block__title">Бонусная программа</div>
                    <div class="feature-block__description">Больше заказов&nbsp;– выше скидка. Баллы с&nbsp;первой покупки. Оплата бонусами до&nbsp;50% стоимости.</div>
                </div>
            </div>
            <div class="feature-block">
                <div class="feature-block__aside">
                    <div class="feature-block__image-box">
                        <img src="/images/media/advantages/advantages_sales.png" alt="" class="feature-block__image">
                    </div>
                </div>
                <div class="feature-block__body">
                    <div class="feature-block__title">Акции</div>
                    <div class="feature-block__description">Регулярные акции и&nbsp;скидки.</div>
                </div>
            </div>
            <div class="feature-block">
                <div class="feature-block__aside">
                    <div class="feature-block__image-box">
                        <img src="/images/media/advantages/advantages_rent.png" alt="" class="feature-block__image">
                    </div>
                </div>
                <div class="feature-block__body">
                    <div class="feature-block__title">Прокат</div>
                    <div class="feature-block__description">Аренда профессионального инструмента для разовых работ.</div>
                </div>
            </div>
        </div>

    </div>
</div>